# Awale Game

## Règles

* Règle 1 : But du jeu Le but du jeu est de s'emparer d'un maximum de graines. Le joueur qui a le plus de graines à la fin de la partie l'emporte.<br>
* Règle 2 : Le terrain de jeu Le terrain de jeu est divisé en deux territoires de 6 trous chacun. Votre territoire est en bas, c'est le territoire Sud; celui de votre adversaire est au dessus, c'est le territoire Nord. Au départ dans les douze trous sont réparties 48 graines (4 par trou).<br>
* Règle 3 : Le tour de jeu. Chaque joueur joue à son tour, celui qui joue en premier est tiré au hasard. Le joueur va prendre l'ensemble des graines présentes dans l'un des trous de son territoire et les distribuer, une par trou, dans le sens inverse des aiguilles d'une montre.<br>
* Règle 4 : Capture Si la dernière graine semée tombe dans un trou de l'adversaire comportant déjà 1 ou 2 graines, le joueur capture les 2 ou 3 graines résultantes. Les graines capturées sont sorties du jeu. (Le trou est alors laissé vide)<br>
* Règle 5: Capture multiple Lorsqu'un joueur s'empare de deux ou trois graines, si la case précédente contient également deux ou trois graines, elle sont capturées aussi, et ainsi de suite.<br>
* Règle 6: Bouclage Si le nombre de graines prises dans le trou de départ est supérieur à 11, cela fait que l'on va boucler un tour : auquel cas, à chaque passage, la case de départ est sautée et donc toujours laissée vide. Un trou contenant assez de graines pour faire une boucle complète s'appelle un Krou.<br>
* Règle 7: Donner à manger On n'a pas le droit d'affamer l'adversaire : De même, un joueur n'a pas le droit de jouer un coup qui prenne toutes les graines du camp de l'adversaire.<br>

## Lancer le jeu

Ouvrir le fichier `index.html` dans votre navigateur