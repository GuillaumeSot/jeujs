let holeEl = document.querySelectorAll('.hole')
let awaleEl = document.querySelector('.awale')

let playerCampNorth = document.querySelector('.player__north')
let playerCampSouth = document.querySelector('.player__south')

let awaleCampNorth = document.querySelector('.awale__north')
let awaleCampSouth = document.querySelector('.awale__south')

const defaultSeed = 4
let tab = []

let playerNorth = false
let playerSouth = false

let playerPointSouth = 0
let playerPointNorth = 0

let playerPointSouthCopy = 0
let playerPointNorthCopy = 0

let pointCopy = 0;

let addition = (a, b) => a + b;
let countTurn = 0;
let disableDisplay = false;

/**
 * End Game
 */
function endGame(){
    if (playerPointNorth >= 24){
        awaleEl.innerHTML = "<h1>Player North has win</h1>"
    }
    if (playerPointSouth >= 24){
        awaleEl.innerHTML = "<h1>Player South has win</h1>"
    }
}

/**
 * Manager of distribution seeds
 *
 * @param {number} index
 * @param {*} value
 */
function manageDistribution(index, value) {
    let beforeDisbtribution = [...tab]

    let lastIndex = distributionSeed(value, index, index)
    getSeed(lastIndex)

    let afterDisbtribution = [...tab]

    starve(beforeDisbtribution, afterDisbtribution)
}

/**
 * If starve
 *
 * @param {*} beforeDisbtribution
 * @param {*[]} afterDisbtribution
 */
function starve(beforeDisbtribution, afterDisbtribution) {
    let tabSplitNorth = []
    let tabSplitSouth = []

    holeEl.forEach((item) => {
        if (isNorth(item)) {
            for(let i = 0; i < 6; i++) {
                tabSplitNorth.push(afterDisbtribution[i])
            }
        } else if(isSouth(item)) {
            for(let i = 6; i < 12; i++) {
                tabSplitSouth.push(afterDisbtribution[i])
            }
        }
    })

    if(tabSplitNorth.reduce(addition) == 0 && playerNorth == true ||
        tabSplitSouth.reduce(addition) == 0 && playerSouth == true) {

        updateDisplay(beforeDisbtribution)
        
        disableDisplay = true
        tab = []
        tab = beforeDisbtribution

        if(playerNorth == false) {
            playerPointNorth = playerPointNorthCopy - pointCopy;
        }
        if(playerSouth == false) {
            playerPointSouth = playerPointSouthCopy - pointCopy;
        }
    } else {
        disableDisplay = false
    }
}

/**
 * Distribution of seeds
 *
 * @param {*} nbSeeds
 * @param {number} index
 * @param {number} startIndex
 */
function distributionSeed(nbSeeds, index, startIndex) {
    tab[index] = 0
    for(let value = nbSeeds; value != 0; value--) {
        index++
        index %= 12

        if(index == startIndex) {
            index++
            index %= 12
        }
        tab[index] += 1
    }
    return index
}

/**
 * Get seeds
 *
 * @param lastIndex
 */
function getSeed(lastIndex) {
    while(isRecoverable(lastIndex) && isAdverse(lastIndex) && disableDisplay == false) {
        distribPoint(lastIndex)
        tab[lastIndex] = 0
        lastIndex--
        if(lastIndex < 0) {
            break
        }
    }
}

/**
 * Distribution of score points for players
 *
 * @param {number} index
 */
function distribPoint(index) {
    if (playerNorth == false && isSouth(holeEl[index])) {
        playerPointNorth += tab[index]
        
    } else if(playerSouth == false && isNorth(holeEl[index])) {
        playerPointSouth += tab[index]
    }

    playerPointNorthCopy = playerPointNorth;
    playerPointSouthCopy = playerPointSouth;

    pointCopy += tab[index]
}

/**
 * If players can collect points
 *
 * @param {number} lastIndex
 */
function isRecoverable(lastIndex) {
    return (tab[lastIndex] == 3 || tab[lastIndex] == 2 ? true : false)
}

/**
 * Is turn adverse
 *
 * @param {number} nbSeeds
 */
function isAdverse(nbSeeds) {
    if (playerNorth == false && isSouth(holeEl[nbSeeds])) {
        return true;
    } else if(playerSouth == false && isNorth(holeEl[nbSeeds])) {
        return true;
    }
    return false;
}

/**
 * If player north
 *
 * @param {EventTarget} params
 */
function isNorth(params) {
    return params.classList.contains('north')
}

/**
 * If player south
 *
 * @param {EventTarget} params
 */
function isSouth(params) {
    return params.classList.contains('south')
}

/**
 * Update seed in hole
 */
function updateDisplay(arr) {
    for (let index = 0; index < arr.length; index++) {
        if(!Number.isNaN(arr[index]) && arr[index] !== undefined) {
            holeEl[index].innerHTML = String(arr[index])
        }
    }
}

/**
 * Disable hole of player
 *
 * @param {boolean|string} turn
 */
function disableHole(turn) {
    playerCampSouth.classList.toggle('disable')
    playerCampNorth.classList.toggle('disable')

    awaleCampSouth.classList.toggle('disable')
    awaleCampNorth.classList.toggle('disable')

    holeEl.forEach((item) => {
        if(playerNorth == false) {
            if(isNorth(item)) {
                item.setAttribute('disable', !turn)
            }
            if(isSouth(item)) {
                item.setAttribute('disable', turn)
            }
        } else if(playerSouth == false) {
            if(isNorth(item)) {
                item.setAttribute('disable', turn)
            }
            if(isSouth(item)) {
                item.setAttribute('disable', !turn)
            }
        }
    })

    for (let index = 0; index < holeEl.length; index++) {
        if (holeEl[index].textContent == 0) {
            holeEl[index].setAttribute('disable', true)
        }
    }
}

/**
 * Player turn
 *
 * @param {Event} e
 */
function turn(e) {
    let attack = false

    if(attack == false) {
        if(isNorth(e.currentTarget)) {
            playerNorth = true
            playerSouth = false
            playerCampSouth.classList.add('disable')
            awaleCampSouth.classList.add('disable')
        } 
        if (isSouth(e.currentTarget)) {
            playerNorth = false
            playerSouth = true
            playerCampNorth.classList.add('disable')
            awaleCampNorth.classList.add('disable')
        }
        disableHole(true)
        attack = true
    } else {
        if(playerNorth == true) {
            disableHole(false)
        }
        if(playerSouth == true) {
            disableHole(false)
        }
    }
}

/**
 * Display point player in dom
 */
function displayPoint() {
    let playerDomPointNorth = document.getElementById('score_north')
    let playerDomPointSouth = document.getElementById('score_south')

    if(playerPointNorth > 0) {
        playerDomPointNorth.innerHTML = playerPointNorth;
    }
    if(playerPointSouth > 0) {
        playerDomPointSouth.innerHTML = playerPointSouth;
    }
}

/**
 * Display count turn in dom
 */
function displayTurn() {
    let turnEl = document.getElementById('nbTurn')
    countTurn++;
    if(countTurn > 0) {
        turnEl.innerHTML = countTurn
    }
}

/**
 * Initial array Tab
 */
function initTab() {
    for(let i = 0; i < 12; i++) {
        tab[i] = defaultSeed
    }
}

/**
 * Initial seed in hole
 */
function initSeed() {
    for (let index = 0; index < holeEl.length; index++) {
        holeEl[index].innerHTML = "4"
    }
}

/**
 * Initial hole
 */
function initHole() {
    initTab()
    initSeed()
    for (let index = 0; index < holeEl.length; index++) {
        holeEl[index].addEventListener('click', (e) => {
            if(holeEl[index].getAttribute('disable') == 'true') {
                e.preventDefault()
            } else {
                manageDistribution(index, tab[index])
                if(disableDisplay === false) {
                    updateDisplay(tab)
                    turn(e)
                    displayPoint()
                    displayTurn()
                } else {
                    e.currentTarget.setAttribute('disable', true)
                }
                endGame()
            }
        })
    }
}

initHole()
